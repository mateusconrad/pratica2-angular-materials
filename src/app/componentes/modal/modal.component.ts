import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-modal',
  // templateUrl: './modal.component.html',
  template: `
    <h1>{{resultado}}</h1>
    `,
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @Input() resultado
  
  constructor() { }

  ngOnInit() {
    console.log(this.resultado)
  }

}
