import { Component } from '@angular/core';

@Component({
  selector: 'dashboard-principal',
  templateUrl: './dashboard-principal.component.html',
  styleUrls: ['./dashboard-principal.component.css']
})
export class DashboardPrincipalComponent {
  cards = [
    { title: 'Cargos X Quantidade Alocado', cols: 1, rows: 1 },
    { title: 'Cargos X Salário X Quantidade Alocado', cols: 1, rows: 1 },
    // { title: 'Card 3', cols: 1, rows: 2 },
    // { title: 'Card 4', cols: 1, rows: 1 }
  ];
}
