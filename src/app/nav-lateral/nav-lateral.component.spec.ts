
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavLateralComponent } from './nav-lateral.component';

describe('NavLateralComponent', () => {
  let component: NavLateralComponent;
  let fixture: ComponentFixture<NavLateralComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NavLateralComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NavLateralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
