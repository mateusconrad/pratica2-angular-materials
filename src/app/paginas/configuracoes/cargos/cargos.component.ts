import { Component, OnInit, Inject } from '@angular/core';
import { CargosService } from 'src/app/servicos/cargos.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Cargos } from 'src/app/model/cargos';
import { MatDialog } from '@angular/material';
import { ModalComponent } from 'src/app/componentes/modal/modal.component';
import { SucessoComponent } from 'src/app/componentes/sucesso/sucesso.component';


@Component({
   selector: 'app-cargos',
   templateUrl: './cargos.component.html',
   styleUrls: ['./cargos.component.css']
})
export class CargosComponent implements OnInit {
   titulonomemodulo: string = "Configurações"
   titulonomepagina: string = "Cadastro de Cargos"
   constructor(private _cadastroCargos: CargosService, private dialog: MatDialog) { }

   ngOnInit() {

      this.load()
      this._cadastroCargos.listar().subscribe(
         (result) => { this.lista = result }
      )
      this.myForm = new FormGroup({
         descricaocargo: new FormControl(),
      }
      )
      // console.log(this.myForm)
   }

   openDialog() {
      const dialogRef = this.dialog.open(ModalComponent);

      dialogRef.afterClosed().subscribe(result => {
         // console.log(`Dialog result: ${result}`);
      });
   }
   myForm: FormGroup
   lista: Cargos[]

   public load() {
      this._cadastroCargos.listar().subscribe(
         (result) => {
            this.lista = result
            console.log(result)
         }
      )
   }
   public save(): void {
      const cli =
         Object.assign({}, this.myForm.value) as Cargos;

      this._cadastroCargos.save(cli).subscribe(
         (result) => {
            this.load();
         },
         (erro) => {
            console.log('erro: ' + erro.message);
         }, () => {
            console.log('Cadastrado');
            this.openDialog()
            // alert('Cadastrado com sucesso!')

         }
      )

      this._cadastroCargos.listar().subscribe(
         (result) => { this.lista = result }
      )
   }
   public delete(cargo?: Cargos) {
      this._cadastroCargos.delete(cargo.idcargo).subscribe(
         (resultado) => {
            console.log('OK');
            this.load();
         }, () => {

         }
      );
   }
}