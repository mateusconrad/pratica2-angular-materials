import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JornadaDeTrabalhoComponent } from './jornada-de-trabalho.component';

describe('JornadaDeTrabalhoComponent', () => {
  let component: JornadaDeTrabalhoComponent;
  let fixture: ComponentFixture<JornadaDeTrabalhoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JornadaDeTrabalhoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JornadaDeTrabalhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
