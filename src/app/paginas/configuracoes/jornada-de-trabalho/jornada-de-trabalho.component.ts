import { Component, OnInit } from '@angular/core';
import { JornadaDeTrabalhoService } from 'src/app/servicos/jornada-de-trabalho.service';
import { FormGroup, FormControl } from '@angular/forms';
import { JornadaDeTrabalho } from 'src/app/model/jornada-de-trabalho';

@Component({
  selector: 'app-jornada-de-trabalho',
  templateUrl: './jornada-de-trabalho.component.html',
  styleUrls: ['./jornada-de-trabalho.component.css']
})
export class JornadaDeTrabalhoComponent implements OnInit {
  titulonomemodulo: string = "Configurações"
  titulonomepagina: string = "Jornada de Trabalho"
  constructor(private _jornadaDeTrabalho: JornadaDeTrabalhoService) { }

  ngOnInit() {

    this.load()
    this._jornadaDeTrabalho.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      ini_manha: new FormControl(),
      fim_manha: new FormControl(),
      ini_tarde: new FormControl(),
      fim_tarde: new FormControl()
    }
    )

    // console.log(this.myForm)
  }
  myForm: FormGroup
  lista: JornadaDeTrabalho[]

  public load() {
    this._jornadaDeTrabalho.listar().subscribe(
      (result) => {
        this.lista = result
        console.log(result)
      }
    )
  }
  public save(): void {
    const cli =
      Object.assign({}, this.myForm.value) as JornadaDeTrabalho;

    this._jornadaDeTrabalho.save(cli).subscribe(
      (result) => {
        this.load();
      },
      (erro) => {
        console.log('erro: ' + erro.message);
      }, () => {
        console.log('Cadastrado');
        alert('Cadastrado com sucesso!')

      }
    )

    this._jornadaDeTrabalho.listar().subscribe(
      (result) => { this.lista = result }
    )
  }
  public delete(jornada?: JornadaDeTrabalho) {
    this._jornadaDeTrabalho.delete(jornada.idhorario).subscribe(
      (resultado) => {
        console.log('OK');
        this.load();
      }, () => {

      }
    );
  }
}
