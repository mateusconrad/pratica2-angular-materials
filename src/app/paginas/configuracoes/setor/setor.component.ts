import { Component, OnInit } from '@angular/core';
import { CadastroSetoresService } from 'src/app/servicos/cadastro-setores.service';
import { FormGroup, FormControl } from '@angular/forms';
import { CadastroSetores } from 'src/app/model/cadastro-setores';
import { Setores } from 'src/app/model/setores';

@Component({
  selector: 'app-setor',
  templateUrl: './setor.component.html',
  styleUrls: ['./setor.component.css']
})
export class SetorComponent implements OnInit {
  titulonomemodulo: string = "Configurações"
  titulonomepagina: string = "Cadastro de Setores"
  constructor(private _cadastroSetores: CadastroSetoresService) { }

  ngOnInit() {

    this.load()
    this._cadastroSetores.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      descricaosetor: new FormControl(),
    }
    )

    // console.log(this.myForm)
  }
  myForm: FormGroup
  lista: CadastroSetores[]

  public load() {
    this._cadastroSetores.listar().subscribe(
      (result) => {
      this.lista = result
        console.log(result)
      }
    )
  }
  public save(): void {
    const cli =
      Object.assign({}, this.myForm.value) as CadastroSetores;

    this._cadastroSetores.save(cli).subscribe(
      (result) => {
        this.load();
      },
      (erro) => {
        console.log('erro: ' + erro.message);
      }, () => {
        console.log('Cadastrado');
        alert('Cadastrado com sucesso!')

      }
    )

    this._cadastroSetores.listar().subscribe(
      (result) => { this.lista = result }
    )
  }
  public delete(setor?: Setores) {
    this._cadastroSetores.delete(setor.idsetor).subscribe(
      (resultado) => {
        console.log('OK');
        this.load();
      }, () => {

      }
    );
  }
}
