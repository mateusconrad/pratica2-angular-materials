import { Component, OnInit } from '@angular/core';
import { CadastroCandidatoService } from 'src/app/servicos/cadastro-candidato.service';
import { FormGroup, FormControl } from '@angular/forms';
import { CadastroCandidato } from 'src/app/model/cadastro-candidato';
import { EstadocivilService } from 'src/app/servicos/estadocivil.service';
import { EstadoCivil } from 'src/app/model/estadocivil';
import { Generos } from 'src/app/model/generos';
import { GenerosService } from 'src/app/servicos/generos.service';

@Component({
   selector: 'app-cadastro-candidato',
   templateUrl: './cadastro-candidato.component.html',
   styleUrls: ['./cadastro-candidato.component.css']
})

export class CadastroCandidatoComponent implements OnInit {
   titulonomemodulo: string = "Recrutamento e Seleção"
   titulonomepagina: string = "Cadastro de Candidato"
   constructor(private _cadastroCandidato: CadastroCandidatoService, private _estadoCivil: EstadocivilService, private _generos: GenerosService) { }

   ngOnInit() {

      this.load()
      this._cadastroCandidato.listar().subscribe(
         (result) => { this.lista = result }
      )
      this.myForm = new FormGroup({
         nomepessoa: new FormControl(),
         datanascimento: new FormControl(),
         contatopessoa: new FormControl(),
         emailpessoa: new FormControl(),
         cpfpessoa: new FormControl(),
         sexopessoa: new FormControl(),
         estadocivil: new FormControl()
      }
      )

      // console.log(this.myForm)
   }
   myForm: FormGroup
   lista: CadastroCandidato[]
   listaestadocivil: EstadoCivil[]
   listagenero: Generos[]

   public load() {
      this._cadastroCandidato.listar().subscribe(
         (result) => {
            this.lista = result
            console.log(result)
         }
      )
      this._estadoCivil.listar().subscribe(
         (result) => {
            this.listaestadocivil = result
            console.log(result)
         }
      )
      this._generos.listar().subscribe(
         (result) => {
            this.listagenero = result
            console.log(result)
         }
      )
   }
   public save(): void {
      const cli =
         Object.assign({}, this.myForm.value) as CadastroCandidato;

      this._cadastroCandidato.save(cli).subscribe(
         (result) => {
            this.load();
         },
         (erro) => {
            console.log('erro: ' + erro.message);
         }, () => {
            console.log('Cadastrado');
            alert('Cadastrado com sucesso!')

         }
      )
      this._generos.listar().subscribe(
         (result) => {
            this.listagenero = result
            console.log(result)
         }
      )

      this._cadastroCandidato.listar().subscribe(
         (result) => { this.lista = result }
      )
   }
   public delete(curriculo?: CadastroCandidato) {
      this._cadastroCandidato.delete(curriculo.idpessoa).subscribe(
         (resultado) => {
            console.log('OK');
            this.load();
         }, () => {

         }
      );
   }
}
