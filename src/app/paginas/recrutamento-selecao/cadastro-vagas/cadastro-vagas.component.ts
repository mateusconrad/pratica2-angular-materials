import { Component, OnInit } from '@angular/core';
import { CadastroVagasService } from 'src/app/servicos/cadastro-vagas.service';
import { FormGroup, FormControl } from '@angular/forms';
import { CadastroVagas } from 'src/app/model/cadastro-vagas'
import { Setores } from 'src/app/model/setores'
import { Cargos } from 'src/app/model/cargos'
import { SetoresService } from 'src/app/servicos/setores.service';
import { CargosService } from 'src/app/servicos/cargos.service';

@Component({
  selector: 'app-cadastro-vagas',
  templateUrl: './cadastro-vagas.component.html',
  styleUrls: ['./cadastro-vagas.component.css']
})
export class CadastroVagasComponent implements OnInit {
  titulonomemodulo: string = "Recrutamento e Seleção"
  titulonomepagina: string = "Cadastro de Vagas"
  constructor(private _cadastroVagas: CadastroVagasService, private _Setores: SetoresService, private _Cargos: CargosService) { }

  ngOnInit() {

    this.load()
    this._cadastroVagas.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      idescolaridade: new FormControl(),
      descricaovaga: new FormControl(),
      atribuicoesvaga: new FormControl(),
      idsetor: new FormControl(),
      idcargo: new FormControl()
    }
    )
  }
  myForm: FormGroup
  lista: CadastroVagas[]
  listacargos: Cargos[]
  listasetores: Setores[]

  public load() {
    this._cadastroVagas.listar().subscribe(
      (result) => {
        this.lista = result
        console.log(result)
      }
    )
    this._Setores.listar().subscribe(
      (resultsetores) => {
        this.listasetores = resultsetores
        console.log(resultsetores)
      }
    )
    this._Cargos.listar().subscribe(
      (resultcargos) => {
        this.listacargos = resultcargos
        console.log(resultcargos)
      }
    )
  }

  public save(): void {
    const cli =
      Object.assign({}, this.myForm.value) as CadastroVagas;

    this._cadastroVagas.save(cli).subscribe(
      (result) => {
        this.load();
      },
      (erro) => {
        console.log('erro: ' + erro.message);
      }, () => {
        console.log('Cadastrado');
        alert('Cadastrado com sucesso!')

      }
    )
    this._cadastroVagas.listar().subscribe(
      (result) => { this.lista = result }
    )
  }
  public delete(vaga?: CadastroVagas) {
    this._cadastroVagas.delete(vaga.idvaga).subscribe(
      (resultado) => {
        console.log('OK');
        this.load();
      }, () => {

      }
    );
  }
}

