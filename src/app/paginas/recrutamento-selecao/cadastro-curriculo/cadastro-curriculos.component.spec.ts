import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroCurriculosComponent } from './cadastro-curriculos.component';

describe('CadastroCurriculoComponent', () => {
  let component: CadastroCurriculosComponent;
  let fixture: ComponentFixture<CadastroCurriculosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroCurriculosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroCurriculosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
