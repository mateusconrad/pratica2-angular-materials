import { Component, OnInit } from '@angular/core';
import { CadastroCurriculosService } from 'src/app/servicos/cadastro-curriculos.service';
import { FormGroup, FormControl } from '@angular/forms';
import { CadastroCurriculos } from 'src/app/model/cadastro-curriculos';
import { CadastroCandidato } from 'src/app/model/cadastro-candidato';
import { CadastroCandidatoService } from 'src/app/servicos/cadastro-candidato.service';
import { CadastroVagasService } from 'src/app/servicos/cadastro-vagas.service';
import { CadastroVagas } from 'src/app/model/cadastro-vagas';

@Component({
   selector: 'app-cadastro-curriculos',
   templateUrl: './cadastro-curriculos.component.html',
   styleUrls: ['./cadastro-curriculos.component.css']
})
export class CadastroCurriculosComponent implements OnInit {
   titulonomemodulo: string = "Recrutamento e Seleção"
   titulonomepagina: string = "Cadastro de Currículos"
   constructor(private _cadastroCurriculos: CadastroCurriculosService, private _cadastroCandidato: CadastroCandidatoService, private _cadastroVagas: CadastroVagasService) { }

   ngOnInit() {

      this.load()
      this._cadastroCurriculos.listar().subscribe(
         (result) => { this.lista = result }
      )
      this.myForm = new FormGroup({
         experienciascurriculo: new FormControl(),
         habilidadescurriculo: new FormControl(),
         pessoa: new FormControl(),
         vagas: new FormControl()
      }
      )

      // console.log(this.myForm)
   }
   myForm: FormGroup
   lista: CadastroCurriculos[]
   listapessoas: CadastroCandidato[]
   listavagas: CadastroVagas[]

   public load() {
      this._cadastroCurriculos.listar().subscribe(
         (result) => {
            this.lista = result
            console.log(result)
         }
      )
      this._cadastroCandidato.listar().subscribe(
         (result) => {
            this.listapessoas = result
            console.log(result)
         }
      )
      this._cadastroVagas.listar().subscribe(
         (result) => {
            this.listavagas = result
            console.log(result)
         }
      )
   }
   public save(): void {
      const cli =
         Object.assign({}, this.myForm.value) as CadastroCurriculos;

      this._cadastroCurriculos.save(cli).subscribe(
         (result) => {
            this.load();
         },
         (erro) => {
            console.log('erro: ' + erro.message);
         }, () => {
            console.log('Cadastrado');
            alert('Cadastrado com sucesso!')

         }
      )

      this._cadastroCurriculos.listar().subscribe(
         (result) => { this.lista = result }
      )
   }
   public delete(curriculo?: CadastroCurriculos) {
      this._cadastroVagas.delete(curriculo.idcurriculo).subscribe(
         (resultado) => {
            console.log('OK');
            this.load();
         }, () => {

         }
      );
   }
}
