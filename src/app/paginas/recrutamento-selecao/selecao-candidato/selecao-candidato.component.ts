import { Component, OnInit } from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material';
export interface PeriodicElement {
  name: string;
  id: number;
}
export interface Evento{
  value: String;
  viewValue: String;
}
const ELEMENT_DATA: PeriodicElement[] = [
  {id: 1, name: 'Fulano de Tal'},
  {id: 2, name: 'Fulano de Tal'},
  {id: 3, name: 'Fulano de Tal'},
  {id: 4, name: 'Fulano de Tal'}
];

@Component({
  selector: 'app-selecao-candidato',
  templateUrl: './selecao-candidato.component.html',
  styleUrls: ['./selecao-candidato.component.css']
})

export class SelecaoCandidatoComponent implements OnInit {
  titulonomemodulo: string = "Recrutamento e Seleção"
  titulonomepagina: string = "Seleção de Candidatos"
  constructor() { }

  ngOnInit() { 
  }
  cards = [
    { title: 'Seleção de Candidatos', cols: 1, rows: 1 }
  ];
  eventos: Evento[] = [
    {value: '1', viewValue: 'Desenvolvimento Web - Abril 2019'}
  ];
  displayedColumns: string[] = ['select', 'id', 'name'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

}
