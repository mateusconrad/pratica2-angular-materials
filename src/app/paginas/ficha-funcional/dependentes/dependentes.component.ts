import { Component, OnInit } from '@angular/core';
import { ModalComponent } from 'src/app/componentes/modal/modal.component';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog, MatGridList } from '@angular/material';
import { Dependentes } from 'src/app/model/cadastro-dependentes';
import { CadastroDepententesService } from 'src/app/servicos/cadastro-depententes.service';
import { CadastroCandidato } from 'src/app/model/cadastro-candidato';
import { CadastroCandidatoService } from 'src/app/servicos/cadastro-candidato.service';

@Component({
  selector: 'app-dependentes',
  templateUrl: './dependentes.component.html',
  styleUrls: ['./dependentes.component.css']
})
export class DependentesComponent implements OnInit {
  titulonomemodulo: string = "Ficha Funcional"
  titulonomepagina: string = "Dependentes"
  constructor(private _cadastroDependentes: CadastroDepententesService, private dialog: MatDialog, private _cadastroCandidato: CadastroCandidatoService) { }

  ngOnInit() {

    this.load()
    this._cadastroDependentes.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      idFunc: new FormControl(),
      tipodependente: new FormControl(),
      nomedependente: new FormControl(),
      rgdependente: new FormControl(),
      cpfdependente: new FormControl()
    }
    )
    // console.log(this.myForm)
  }

  openDialog() {
    const dialogRef = this.dialog.open(ModalComponent);

    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }
  myForm: FormGroup
  lista: Dependentes[]
  listafuncionario: CadastroCandidato[]

  public load() {
    this._cadastroDependentes.listar().subscribe(
      (result) => {
        this.lista = result
        console.log(result)
      }
    )
    this._cadastroCandidato.listar().subscribe(
      (result) => {
         this.listafuncionario = result
         console.log(result)
      }
   )
  }
  public save(): void {
    const cli =
      Object.assign({}, this.myForm.value) as Dependentes;

    this._cadastroDependentes.save(cli).subscribe(
      (result) => {
        this.load();
      },
      (erro) => {
        console.log('erro: ' + erro.message);
      }, () => {
        console.log('Cadastrado');
        this.openDialog()
        // alert('Cadastrado com sucesso!')

      }
    )

    this._cadastroDependentes.listar().subscribe(
      (result) => { this.lista = result }
    )
    this._cadastroCandidato.listar().subscribe(
      (result) => { this.listafuncionario = result }
    )
  }
  public delete(funcionario?: Dependentes) {
    this._cadastroDependentes.delete(funcionario.iddependente).subscribe(
       (resultado) => {
          console.log('OK');
          this.load();
       }, () => {

       }
    );
 }
}
