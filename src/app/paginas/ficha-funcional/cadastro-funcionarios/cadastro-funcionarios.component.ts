import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CadastroCandidatoService } from 'src/app/servicos/cadastro-candidato.service';
import { CadastroFuncionariosService } from 'src/app/servicos/cadastro-funcionarios.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ModalComponent } from 'src/app/componentes/modal/modal.component';
import { Cargos } from 'src/app/model/cargos';
import { CadastroFuncionarios } from 'src/app/model/cadastro-funcionarios';
import { Setores } from 'src/app/model/setores';
import { CargosService } from 'src/app/servicos/cargos.service';
import { SetoresService } from 'src/app/servicos/setores.service';
import { CadastroCandidato } from 'src/app/model/cadastro-candidato';

@Component({
  selector: 'app-cadastro-funcionarios',
  templateUrl: './cadastro-funcionarios.component.html',
  styleUrls: ['./cadastro-funcionarios.component.css']
})
export class CadastroFuncionariosComponent implements OnInit {
  titulonomemodulo: string = "Ficha Funcional"
  titulonomepagina: string = "Cadastro Funcionários"
  constructor(private _cadastroFuncionarios: CadastroFuncionariosService, 
    private dialog: MatDialog, 
    private _cadastroCandidato: CadastroCandidatoService, 
    private _cargos: CargosService,
    private _setores: SetoresService
    ) { }

  ngOnInit() {

    this.load()
    this._cadastroFuncionarios.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      idpessoa: new FormControl(),
      idcargo: new FormControl(),
      idsetor: new FormControl(),
      endereco: new FormControl(),
      telefone: new FormControl(),
      numerodependentes: new FormControl(),
      salario: new FormControl()
    }
    )
    // console.log(this.myForm)
  }
  openDialog() {
    const dialogRef = this.dialog.open(ModalComponent);

    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }
  myForm: FormGroup
  lista: CadastroFuncionarios[]
  listacargos: Cargos[]
  listasetor: Setores[]
  listapessoa: CadastroCandidato[]

  public load() {
    this._cadastroFuncionarios.listar().subscribe(
      (result) => {
        this.lista = result
        console.log(result)
      }
    )
    this._cargos.listar().subscribe(
      (result) => {
        this.listacargos = result
        console.log(result)
      }
    )
    this._setores.listar().subscribe(
      (result) => {
        this.listasetor = result
        console.log(result)
      }
    )
    this._cadastroCandidato.listar().subscribe(
      (result) => {
        this.listapessoa = result
        console.log(result)
      }
    )
  }
  public save(): void {
    const cli =
      Object.assign({}, this.myForm.value) as CadastroFuncionarios;

    this._cadastroFuncionarios.save(cli).subscribe(
      (result) => {
        this.load();
      },
      (erro) => {
        console.log('erro: ' + erro.message);
      }, () => {
        console.log('Cadastrado');
        this.openDialog()
        // alert('Cadastrado com sucesso!')
      }
    )

    this._cadastroFuncionarios.listar().subscribe(
      (result) => { this.lista = result }
    )
    this._cargos.listar().subscribe(
      (result) => { this.listacargos = result }
    )
    this._setores.listar().subscribe(
      (result) => { this.listasetor = result }
    )
    this._cadastroCandidato.listar().subscribe(
      (result) => { this.listapessoa = result }
    )
  }
  public delete(funcionario?: CadastroFuncionarios) {
    this._cadastroFuncionarios.delete(funcionario.idfuncionario).subscribe(
       (resultado) => {
          console.log('OK');
          this.load();
       }, () => {

       }
    );
 }

}
