import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Generos } from '../model/generos';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GenerosService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<Generos[]> {
    return this._http
      .get<Generos[]>("http://localhost:8080/genero")
  } 
  public save(generos: Generos): Observable<any> {
    return this._http
      .post("http://localhost:8080/genero", generos)
  }
}
