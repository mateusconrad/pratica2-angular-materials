import { TestBed } from '@angular/core/testing';

import { JornadaDeTrabalhoService } from './jornada-de-trabalho.service';

describe('JornadaDeTrabalhoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JornadaDeTrabalhoService = TestBed.get(JornadaDeTrabalhoService);
    expect(service).toBeTruthy();
  });
});
