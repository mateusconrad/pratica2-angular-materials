import { TestBed } from '@angular/core/testing';

import { CadastroCandidatoService } from './cadastro-candidato.service';

describe('CadastroCandidatoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CadastroCandidatoService = TestBed.get(CadastroCandidatoService);
    expect(service).toBeTruthy();
  });
});
