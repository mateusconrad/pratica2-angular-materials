import { TestBed } from '@angular/core/testing';

import { CadastroVagasService } from './cadastro-vagas.service';

describe('CadastroVagasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CadastroVagasService = TestBed.get(CadastroVagasService);
    expect(service).toBeTruthy();
  });
});
