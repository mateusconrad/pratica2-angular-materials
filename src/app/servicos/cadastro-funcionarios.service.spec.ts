import { TestBed } from '@angular/core/testing';

import { CadastroFuncionariosService } from './cadastro-funcionarios.service';

describe('CadastroFuncionariosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CadastroFuncionariosService = TestBed.get(CadastroFuncionariosService);
    expect(service).toBeTruthy();
  });
});
