import { TestBed } from '@angular/core/testing';

import { CadastroSetoresService } from './cadastro-setores.service';

describe('CadastroSetoresService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CadastroSetoresService = TestBed.get(CadastroSetoresService);
    expect(service).toBeTruthy();
  });
});
