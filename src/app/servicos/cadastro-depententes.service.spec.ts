import { TestBed } from '@angular/core/testing';

import { CadastroDepententesService } from './cadastro-depententes.service';

describe('CadastroDepententesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CadastroDepententesService = TestBed.get(CadastroDepententesService);
    expect(service).toBeTruthy();
  });
});
