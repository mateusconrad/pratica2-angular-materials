import { TestBed } from '@angular/core/testing';

import { CadastroCurriculosService } from './cadastro-curriculos.service';

describe('CadastroCurriculosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CadastroCurriculosService = TestBed.get(CadastroCurriculosService);
    expect(service).toBeTruthy();
  });
});
