import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecrutamentoSelecaoComponent } from './paginas/recrutamento-selecao/recrutamento-selecao.component';
import { CadastroVagasComponent } from './paginas/recrutamento-selecao/cadastro-vagas/cadastro-vagas.component';
import { CadastroCandidatoComponent } from './paginas/recrutamento-selecao/cadastro-candidato/cadastro-candidato.component';
import { HomeComponent } from './paginas/home/home.component';
import { SelecaoCandidatoComponent } from './paginas/recrutamento-selecao/selecao-candidato/selecao-candidato.component';
import { CadastroCurriculosComponent } from './paginas/recrutamento-selecao/cadastro-curriculo/cadastro-curriculos.component';
import { CargosComponent } from './paginas/configuracoes/cargos/cargos.component';
import { SetorComponent } from './paginas/configuracoes/setor/setor.component';
import { DependentesComponent } from './paginas/ficha-funcional/dependentes/dependentes.component';
import { CadastroFuncionariosComponent } from './paginas/ficha-funcional/cadastro-funcionarios/cadastro-funcionarios.component';
import { JornadaDeTrabalhoComponent } from './paginas/configuracoes/jornada-de-trabalho/jornada-de-trabalho.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'recrutamento-selecao', component: RecrutamentoSelecaoComponent },
  { path: 'cadastro-evento-recrutamento', component: CadastroVagasComponent },
  { path: 'cadastro-candidato', component: CadastroCandidatoComponent },
  { path: 'selecao-candidato', component: SelecaoCandidatoComponent },
  { path: 'cadastro-curriculo', component: CadastroCurriculosComponent },
  { path: 'cargos', component: CargosComponent },
  { path: 'setores', component: SetorComponent},
  { path: 'dependentes', component: DependentesComponent},
  { path: 'funcionarios', component: CadastroFuncionariosComponent},
  { path: 'jornada-de-trabalho', component: JornadaDeTrabalhoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
