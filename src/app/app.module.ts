import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavLateralComponent } from './nav-lateral/nav-lateral.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatCheckboxModule, MatSelectModule, MatRippleModule, MatInputModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatGridListModule, MatCardModule, MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MatProgressBarModule } from '@angular/material';
import { DashboardPrincipalComponent } from './dashboard-principal/dashboard-principal.component';
import { TabelaComponent } from './tabela/tabela.component';
import { RecrutamentoSelecaoComponent } from './paginas/recrutamento-selecao/recrutamento-selecao.component';
import { HomeComponent } from './paginas/home/home.component';
import { CadastroVagasComponent } from './paginas/recrutamento-selecao/cadastro-vagas/cadastro-vagas.component';
import { CadastroCandidatoComponent } from './paginas/recrutamento-selecao/cadastro-candidato/cadastro-candidato.component';
import { SelecaoCandidatoComponent } from './paginas/recrutamento-selecao/selecao-candidato/selecao-candidato.component';
import { MatFileUploadModule } from 'angular-material-fileupload';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CadastroCandidatoService } from './servicos/cadastro-candidato.service';
import { CadastroFuncionariosComponent } from './paginas/ficha-funcional/cadastro-funcionarios/cadastro-funcionarios.component';
import { ListaFuncionariosComponent } from './paginas/ficha-funcional/lista-funcionarios/lista-funcionarios.component';
import { HorasFaltasComponent } from './paginas/folha-pagamento/horas-faltas/horas-faltas.component';

import { TabelaDescontosComponent } from './paginas/folha-pagamento/tabela-descontos/tabela-descontos.component';
import { DependentesComponent } from './paginas/ficha-funcional/dependentes/dependentes.component';
import { CadastroCurriculosComponent } from './paginas/recrutamento-selecao/cadastro-curriculo/cadastro-curriculos.component';
import { CargosComponent } from './paginas/configuracoes/cargos/cargos.component';
import { SetorComponent } from './paginas/configuracoes/setor/setor.component';
import { ModalComponent } from './componentes/modal/modal.component';
import { SucessoComponent } from './componentes/sucesso/sucesso.component';
import { JornadaDeTrabalhoComponent } from './paginas/configuracoes/jornada-de-trabalho/jornada-de-trabalho.component';
import { Grafico1Component } from './componentes/grafico1/grafico1.component';


@NgModule({
  declarations: [
    AppComponent,
    NavLateralComponent,
    DashboardPrincipalComponent,
    TabelaComponent,
    RecrutamentoSelecaoComponent,
    HomeComponent,
    CadastroVagasComponent,
    CadastroCandidatoComponent,
    SelecaoCandidatoComponent,
    CadastroFuncionariosComponent,
    ListaFuncionariosComponent,
    HorasFaltasComponent,
    TabelaDescontosComponent,
    DependentesComponent,
    CadastroCurriculosComponent,
    CargosComponent,
    SetorComponent,
    ModalComponent,
    SucessoComponent,
    JornadaDeTrabalhoComponent,
    Grafico1Component
  ],
  exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
  ],
  imports: [
    MatCheckboxModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatFileUploadModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatProgressBarModule
  ],
  providers: [CadastroCandidatoService, {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}],
  entryComponents: [ModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
