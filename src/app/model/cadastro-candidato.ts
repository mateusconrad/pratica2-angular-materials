export interface CadastroCandidato{
    idpessoa: number;
    nomepessoa?: string;
    dataNascimento?: Date;
    telefone?: number;
    emailpessoa?: String;
    contatopessoa?: String;
    sexopessoa?: number;
    cpfpessoa?: number;
    idestadocivil?: number;
}