export interface JornadaDeTrabalho{
    idhorario: number
    ini_manha?: any
    fim_manha?: any
    ini_tarde?: any
    fim_tarde?: any
}