export interface CadastroVagas{
    idvaga: number
    descricaovaga?: string
    atribuicoesvaga?: string
    idsetor?: number
    idcargo?: number
    idtiporecrutamento?: number
}