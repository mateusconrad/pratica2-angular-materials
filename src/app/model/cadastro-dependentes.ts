export interface Dependentes{
    idFunc: number
    iddependente?: number
    tipodependente?: number
    nomedependente?: string
    rgdependente?: string
    cpfdependente?: string
    datanascimentodependente?: string
    depir?: any
}